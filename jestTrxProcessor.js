const builder = require('jest-trx-results-processor');

const processor = builder({
    outputFile: 'jestTestresults.trx' 
});

module.exports = processor;