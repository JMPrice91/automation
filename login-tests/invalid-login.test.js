const Nightmare = require("nightmare");
const nightmare = Nightmare({ show: false });

const email = "jamesm.price@hotmail.co.uk";
const day = "9";
const month = "3";
const year = "91";

describe('As a returning customer', async () => {

    test("I should NOT be able to login with in-valid credentials", async () => {
        
        // Arrange
        jest.setTimeout(15000);
        const expected = 'Retrieve a quote from Gocompare.com';
        const password = "james1980";

        // Act
        var result = await loginViaHomePage(password);

        // Assert
        expect(result).toEqual(expected);
    });

});

const loginViaHomePage = async (password) => {
        try {
            const result = 
                await nightmare
                .goto("https://www.gocompare.com")
                .wait(".account")
                .click(".account > a")
                .wait(".questionsection > h2")
                .insert("#Email", email)
                .insert("#Password", password)
                .insert("#DateOfBirth_Day", day)
                .insert("#DateOfBirth_Month", month)
                .insert("#DateOfBirth_Year", year)
                .click(".btn-sign-in")
                .evaluate( async () => 
                    await document.title
                )
                .end();
        
            return result;
        } 
        catch (e) {
            console.error(e);
            return undefined;
        }
}