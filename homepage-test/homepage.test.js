const Nightmare = require("nightmare");

const nightmare = Nightmare({
  show: false,
  width: 1200,
  height: 1024
});

const expected =
  "GoCompare™ | Quick & Easy Quotes in Minutes - Official Website";
const newImage = "homepage-full-" + Math.random() + ".png";

test("title should be: " + expected, async () => {
  jest.setTimeout(15000);

  const result = await goToHomepage();
  expect(result).toEqual(expected);
});

goToHomepage = async () => {
  try {
    const dimentions = await nightmare
      .goto("https://www.gocompare.com")
      .wait("body")
      .evaluate( async () => {
        var body = await document.querySelector("body");
        return {
          width: body.scrollWidth,
          height: body.scrollHeight
        };
      });

    const title = await nightmare
      .viewport(dimentions.width, dimentions.height)
      .screenshot(newImage)
      .evaluate(async () => {
        return await document.title;
      })
      .end();

    return title;

  } catch (e) {
    console.error(e);
    return undefined;
  }
};
